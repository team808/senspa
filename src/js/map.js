ymaps.ready(function () {
    var option = {
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: 'img/logo-map.png',
        // Размеры метки.
        iconImageSize: [59, 67],
        // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
        iconImageOffset: [-32, -33]
    };

    var myMap = new ymaps.Map('map', {
        center: [55.771793, 37.708141],
        zoom: 16
    }, {
        searchControlProvider: 'yandex#search'
    }),

    myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'г. Москва, Госпитальный Вал, д. 8/1, стр. 2',
    }, option);


    var myMap2 = new ymaps.Map('map2', {
        center: [55.771793, 37.708141],
        zoom: 16
    }, {
        searchControlProvider: 'yandex#search'
    }),
    myPlacemark2 = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'г. Москва, Госпитальный Вал, д. 8/1, стр. 2',
    }, option);

    myMap.geoObjects.add(myPlacemark);
    myMap2.geoObjects.add(myPlacemark2);

});
