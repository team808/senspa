document.addEventListener('DOMContentLoaded', function(){

  if(document.getElementById('map')) {

    ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [45.023781, 39.084845],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'г. Краснодар, ул. Карасунская, д. 60 ДЦ «Карасунский», 2 эт., оф. 25',
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: './img/point.png',
            // Размеры метки.
            iconImageSize: [46, 60],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-23, -60]
        });

    myMap.geoObjects.add(myPlacemark);

  });

}

});
